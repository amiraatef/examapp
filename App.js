import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import {Routes} from './src/Routes'
import {Provider} from 'react-redux';
import store from './src/Reducers/store'
import { Font , AppLoading} from 'expo';


export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { loading: true };
    }

  async componentWillMount() {
    await Font.loadAsync({
    Roboto: require("native-base/Fonts/Roboto.ttf"),
    Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
    });
    this.setState({ loading: false });
    }
    
  render() {
    console.disableYellowBox = true;

    if (this.state.loading) {
      return (<View>
        </View>
      );
      }
    return (
      <Provider store={store}>
               <Routes/>
               </Provider>
    );
  }
}

