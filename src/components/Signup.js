import React, { Component } from 'react'
import {Button,View,Text} from 'native-base'
 import GenerateForm from 'react-native-form-builder'
import { styles } from './Styles';
const fields = [
  {
    type: 'text',
    name: 'UserName',
    required: true,
    icon: 'ios-person',
    label: 'User Name',
   
  },
  {
    type: 'text',
    name: 'Email',
    required: true,
    icon: 'ios-person',
    label: 'Email',
   
  },
  {
    type: 'password',
    name: 'password',
    icon: 'ios-lock',
    required: true,
    label: 'Password',
  },
  {
    type: 'password',
    name: 'ConfirmPassword',
    icon: 'ios-lock',
    required: true,
    label: 'ConfirmPassword',
  },
  
];
export class SignUp extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
       Fromerror:false,
       FromerrorMsg:''
    };
  };
  
  
    validate=(field)=>{
      let error = false;
      let errorMsg = '';
  
      if (field.name === 'Email' && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(field.value)) {
        error = true;
        errorMsg = 'Invalid email address';
      }
  
      this.setState({Fromerror:true})
      return { error, errorMsg };
    }
    Signup() {
  if(this.state.Fromerror)
  {
    return
  }else{
      let Newuser = {
        Email: "",
        Password: "",
        UserName:"",
        ConfirmPassword:''


      }
      NewUser = this.formGenerator.getValues();
      let email = NewUser.Email
      let password = NewUser.password
      let confirmpassword=Newuser.ConfirmPassword
//passvalidation
      if(!email.match(/\S/g)||!password.match(/\S/g))
      {
        if(!this.state.FromerrorMsg.match(/\S/g))
        {
        this.setState({FromerrorMsg:'Invaild Email or Password'})
        }
        else{
        this.setState({FromerrorMsg:''})
        }
        return
  
      }
    }
  }
    render() {
      return (
        <View style={styles.container}>
          <View>
    <GenerateForm ref={(c) => {this.formGenerator = c;}}fields={fields} customValidation={(field)=>this.validate(field)} />     
            </View>
                <View style={styles.BtnView}>
            <Button style={styles.submitButton} onPress={() => this.Signup()}>
              <Text style={styles.txtbtn} > Signup</Text>
            </Button>
            <Text>{this.state.FromerrorMsg}</Text>
          </View>
        </View>
      )
    }
}
