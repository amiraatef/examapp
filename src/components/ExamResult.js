import React, { Component } from 'react'
import {View} from 'react-native'
import { Table, Row, Rows } from 'react-native-table-component';
import {styles} from './Styles'
export class ExamResult extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tableHead: ['Student Name', 'Math /50', 'English /20'],
      tableData: [
        ['1', '2', '3'],
        ['a', 'b', 'c'],
      ]
    }
  }
  render() {
    const state = this.state;
    return (
      <View style={styles.container}>
      <Table borderStyle={{borderWidth: 2, borderColor: '#c8e1ff'}}>
        <Row data={state.tableHead} style={styles.head} textStyle={styles.text}/>
        <Rows data={state.tableData} textStyle={styles.text}/>
      </Table>
    </View>
    )
  }
}
