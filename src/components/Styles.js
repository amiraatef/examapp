import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({
    container: {
        flex: 1,
    justifyContent: 'center',
    alignContent: 'center',
    flexDirection: 'column'
      },

      BtnView:{
        flex:.5,
        flexDirection: 'column',
        alignContent: 'center',
justifyContent:'space-around'
      },
      submitButton: {
marginLeft: '30%',
width: '40%',
borderRadius: 10,

      },
      txtbtn:{
marginLeft:'25%' ,     },
headerBackground: {
      flex: 1,
      alignItems: 'stretch',
backgroundColor:'white'
  },
  profilepicWrap: {
      width: 150,
      height: 200,
      borderRadius: 100,
      borderColor: 'rgba(0,0,0,0.4)',
      borderWidth: 14,

  },

  header: {
      flex: 1,
      alignItems: 'center',
      backgroundColor: 'rgba(0,0,0,0.5)',
      padding:'10%'
  }, name: {

      marginTop:'5%',
      fontSize: 16,
      color: '#fff',
      fontWeight: 'bold'

  },
  profilepic: {
      flex: 1,
      alignSelf: 'center',
      borderColor: '#fff',
      height: 200,
      width: 150,
      borderWidth: 4,
      borderRadius: 75,
      resizeMode: 'cover'
  },

  pos: {
      fontSize: 14,
      color: '#0394c0',
      fontWeight: '300',
      fontStyle: 'italic'
  },
  footertab:{
    backgroundColor: "#FFF"
  },
  UserProfileTab:{ 
      paddingLeft: 0, paddingRight: 0, borderRadius: 100
     },
     container: { flex: 1, padding: 16, paddingTop: 30, backgroundColor: '#fff' },
     head: { height: 40, backgroundColor: '#f1f8ff' },
     text: { margin: 6 }
})
