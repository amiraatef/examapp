import React, { Component } from 'react'
import {Button,View,Text} from 'native-base'
 import GenerateForm from 'react-native-form-builder'
 import {styles} from './Styles'
 const fields = [
  {
    type: 'text',
    name: 'Email',
    required: true,
    icon: 'ios-person',
    label: 'Email',
   
  },
  {
    type: 'password',
    name: 'password',
    icon: 'ios-lock',
    required: true,
    label: 'Password',
  },
  
];
export class Login extends Component {
constructor(props) {
  super(props)

  this.state = {
     Fromerror:false,
     FromerrorMsg:'',
     
  };
};


  validate=(field)=>{
    let error = false;
    let errorMsg = '';

    if (field.name === 'Email' && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(field.value)) {
      error = true;
      errorMsg = 'Invalid email address';
    }

    this.setState({Fromerror:true})
    return { error, errorMsg };
  }
  Login() {
    let Newuser = {
      Email: "",
      password: "",
    }
    Newuser = this.formGenerator.getValues();
    let email = Newuser.Email
    let password = Newuser.password
    if(!email.match(/\S/g))
    {
      if(!this.state.FromerrorMsg.match(/\S/g))
      {
      this.setState({FromerrorMsg:'Invaild Email or Password'})
      }
      else{

      this.setState({FromerrorMsg:''})
      }
      return
  }
  this.props.navigation.navigate('Home')
}
  render() {
    return (
      <View style={styles.container}>
        <View>
  <GenerateForm ref={(c) => {this.formGenerator = c;}}fields={fields} customValidation={(field)=>this.validate(field)} />     
          </View>
              <View style={styles.BtnView}>
          <Button style={styles.submitButton} onPress={() => this.Login()}>
            <Text style={styles.txtbtn} >Login</Text>
          </Button>
          <Button style={styles.submitButton} onPress={() =>this.props.navigation.navigate('SignUp')}>
            <Text style={styles.txtbtn} >SignUp</Text>
          </Button>
          
          <Text>{this.state.FromerrorMsg}</Text>
        </View>
      </View>
    )
  }
}
