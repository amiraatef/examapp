const initialState = {
    UserprofileClicked:true,
    ExamsResultclicked:false
}
const reducer = (state = initialState, action) => {
    if (action.type == 'profileOn') {
        return {
            ...state,
            UserprofileClicked:true,
            ExamsResultclicked:false
        }
    }
    else if (action.type === 'examResultOn') {
        return {
            ...state,
            ExamsResultclicked:true,
            UserprofileClicked:false,
        }
    }

        return state;
}

export default reducer;