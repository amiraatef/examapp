import {DrawerNavigator,DrawerItems} from 'react-navigation';
import React from 'react'
import Meteor from 'react-native-meteor';
Meteor.connect('ws://localhost:3000/websocket');
import{Login,SignUp,Home} from './components/index'
export const Routes = DrawerNavigator({
  Login: { screen: Login },
  Home:{screen:Home},
  SignUp:{screen: SignUp}
},
{
contentComponent:customDrawerComponent});
const customDrawerComponent=(this.props)=(

<DrawerItems {...this.props} />
)
